[Home](../README.md)


# Table of Contents
- [Preparation](#preparation)
- [Servicing Order for Cash withrawal Sequence Diagram](#servicing-order-for-cash-withrawal-sequence-diagram)
- [Development](#development)

---

## Preparation  

VScode Extention

```
Extention Pack for JAva
Spring Boot Extension Pack
```

- Gitlab pull  
```

```


- cli 설정  

``` 
npx k5 setup --file cli-config.json
```

- k5 clone   

```  
npx k5 clone -s isw-enablement-project/{YPUR_PROJECT_NAME} -p "gitlab.com"  
```  

- Project source code structure  

  | folder/file name | functions |
  |-----------------------|-------------------------------------------------------------|
  | src-design | Solution Design에서 작성한 Desgin 정보|
  | svcord-application | 실제 서비스를 구성하는 source code |
  | solution.yml | 해당 project의 meta정보|
  | extention-values.yaml | Openshift의 secret과 configMap을 이용하기 위한 env 설정파일 |


- Working folder  
  svcord-application/src/main/java/com/ibm/svcord  

  | foler name | functions |
  |-------------------------------|----------------------------------------------------|
  | api/v1 | API layer에서 설계한 내용의 로직 작성 |
  | domain/{DomainNamespace} | Domain layer에서 설계한 내용의 로직 작성 |
  | domain/{DomainNamespace}/command | Domain layer의 Command에 설계한 내용의 로직 작성 |
  | domain/{DomainNamesapce}/service | Domain layer의 Service에 설계한 내용의 로직 작성 |
  | integration/{IntegrationNamespace} | Integration layer에서 설계한 내용 로직 작성 - 각 namespace별 생성 | 




## Servicing Order for Cash withrawal Sequence Diagram

![servicing order](images/servicingorder.png)  

1. POST /withrawal로 현금인출에 대한 요청을 받음  
2. API layer에서 실제 로직을 관장하는 Service를 호출 함  
3. Service layer에서 위 그림에 보이는 로직을 수행 함
  - integration의 Party Life cycle Management 호출  
  - 사용자 ID의 validation을 요청한 후 결과를 보내 줌  
  - Command를 이용하여 Servicing order가 시작 됨을 생성 함 (새로운 프로시저 시작)
  - 현금결제 요청을 위해 Integration의 Payment Order를 호출 함 - 앞에서 받았던 정보와 현재 서비스의 정보를 같이 전달
  - Payment Order에서 받은 결과를 바탕으로 프로시저를 업데이트 하고 이를 API layer로 보내 줌  
4. Service Layer 의 결과를 API layer의 POST /withdrawal의 결과로 전달 함  


## Development  

### SpringBoot  
Java Spring Boot는 Java 기반 프레임워크를 사용하여 마이크로 서비스 및 웹 앱을 보다 쉽게 만들 수 있는 오픈 소스 도구입니다.   
리소스에 대한 사용을 매우 용이하게 하는 장점이 있어, 우리가 해당 서비스에 DB를 붙이던지, Message queue를 붙이는 부분들이 쉽게 적용 될 수 있고,  
코드 상에서도 이를 쉽게 사용할 수 있게 Annotation들을 제공하고 있습니다.  

- ISW는 java project 생성시 Spring Boot기반의 서비스를 생성해 주며, 이 때 기본적으로 서비스가 배포되어 사용될 때 필요한 configuration은 자동 생성해 줍니다.  
  예를 들어 keyclock 이라든지, Kafka에 대한 configuration이라든지, DB설정 사항등은(MongoDB only)  추가적으로 구성 해 줄 필요가 없습니다.  

- 엔터프라이즈 레벨에서 개발에 대한 표준화를 만들 때 application의 source code 구조 및 naming rules, Configuration의 방식 등에 대해 크게 고민 하지 않아도,  
  generated된 소스코드 위에 필요한 로직만 추가하여 만들 수 있기 때문에 마이크로서비스 세계로 쉽게 갈 수 있게 도와 줍니다.  
### API 로직 개발  
API Provider는 Service를 호출 하여 해당 endpoint에 맞는 동작을 지원함  

1. Autowired for Service and entity Builder
 - 각 클래스에서 사용하는 서비스는(Annotation을 호출하기 위해서는) @Autowired를 이용하여 부른다. Annotation으로 이루어진 class는 인스턴스를 하나만 생성하여 쓰기 때문에
 생성된 것을 가져다 쓰겠다는 의미 임.

```
  @Autowired
  WithdrawalDomainService withdrawaDomainService;

  @Autowired
  DomainEntityBuilder entityBuilder;
````  


2. Service 호출
  -  API호출 시 받은 데이터(Request body)를 서비스를 호출하기 위한 Input에 매칭 하고 호출
     이 때 InputData는  entityBuilder를 통해 Setting 및 building을 해 준다. - build는 새로운 instnace생성
  - 서비스 호출 후 받은 결과를 API의 호출에 대한 Response를 생성하여 return한다.


### Service 로직 개발
API layer로 부터 호출 되어, 해당 endpoint에 대한 실제 동작을 정의 함

1. 사용하기 위한 Command, Intergartion services등을 Autowired로 엮는다.
```
  @Autowired
  ServicingOrderProcedureCommand servicingOrderProcedureCommand;

  @Autowired
  PaymentOrder paymentOrder;

  @Autowired
  RetrieveLogin retrieveLogin;
```
2. execution함수안에는 다음과 같은 로직이 구성되야 한다.
  1) 사용자 인증요청 Party Life Cycle Management 호출
  2) User Valid 체크
  3) Root Entity 호출하여 새로운 레코드 생성
  4) 현금 출금 요청을 위한  Payment Order 호출
  5) Payment order결과를 DB에 업데이트 하고 해당 아웃풋을 전달 한다.

### Command  로직 개발
DB에 저장되어 잇는 데이터의 Creation,  update, delete의 방법을 정의한다.  
JPA가 기본적으로 들어가 있어  ORM 을 사용 가능하게 란다. 여기에서는 save를 통해 creation과 update를, delete를 통해 삭제.
 - ISW에서 생성하는 repo를 이용하여 사용 가능

### Integration 구성
   전달 받은 데이터를 이용하여 다른 서비스를 호출 하고, 받은 결과를 Service로 전달 합.   
   namespace별 directory, endpoint 별 class 생성.  
   Party lifecycle Management에서는 사용자에 대한 인증 결과를  
   Payment Order 에서는 payment order를 생성하기 위한 요청 및 결과를 전달 하게 됨.  


### 추가 환경 설정
Openshift내 ConfigMap이나 Secret 사용시 작성. 
우리는  RDBMS를 사용하기 때문에 이에 대한 id/pw/DB이름을  Secret이나 ConfigMap으로 작성하여 관린 할 수 있음.  
extension-values.yaml 안에 Secret추가 작업  
```
env:
  variables:
    secretKeyRef:
      - variableName: DB_URL
        secretName: k5-db-credential
        secretKey: url
        optional: false
      - variableName: DB_PASSWORD
        secretName: k5-db-credential
        secretKey: password
        optional: false
      - variableName: DB_USER
        secretName: k5-db-credential
        secretKey: id
        optional: false
```

## Ready to Deploy
- compile
```
npx k5 compile
``

- gitlab에 로직이 반영된 코드 반영
```
npx k5 push -m "commit comment" 
```




  

